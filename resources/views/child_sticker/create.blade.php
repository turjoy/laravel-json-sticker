@extends('layouts.app')

@section('content')

    <div class="container">
        <h2>Add a New Sticker</h2>
        {!! Form::open([
                'url' => '/sticker/sticker-form/'.$getBundleName,
                'class' => 'form',
                'novalidate' => 'novalidate',
                'files' => true
            ]) !!}
        <div class="form-group">
            {!! Form::label('Sticker Name') !!}
            {!! Form::text('sticker_name', null, ["class"=>"form-control"]) !!}
        </div>

        <div class="form-group">
            {!! Form::label('Sticker Icon') !!}
            {!! Form::file('sticker_icon', null) !!}
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
        {!! Form::close() !!}
    </div>

@endsection