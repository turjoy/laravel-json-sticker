@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Thumbnails</div>
                </div>
                <ul id="sortable">
                    @if($json == [])
                        <p class="alert alert-info">No Bundles found.</p>
                    @else
                        <?php $i = 0; ?>
                        @foreach($json as $data)
                            <li id="{{$i}}" class="ui-state-default" >
                                <a href="sticker/stickers/{{$i}}">
                                    <img class="img-responsive" src="{{$data['bundle_icon']}}"  title="{{ $data['bundle_name'] }}" style="height: 100%; width: 100%" >
                                </a>
                            </li>
                            <?php $i++; ?>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <a href="sticker/bundle-form" class="btn btn-default btn-block">Add A New Bundle</a>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <script>

        $(document).ready(function ()
        {
            $('ul').sortable({
                axis: 'x, y',
                update: function (event, ui) {
                    var data = $(this).sortable('toArray');
                    var token = '{{csrf_token()}}';
                    $.ajax({
                        method: 'POST',
                        data: {'data': data, '_token': token},
                        type: 'POST',
                        url: '/sticker/bundle-sortable',
                        success: function(){
                            var index = 0;
                            $("#sortable").find('li').each(function(){
                                $(this).attr('id', index);
                                index++;
                            });
                        }
                    });
                }
            });
        });

    </script>

@endsection