@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><strong>Bundle Name: </strong>{{$oneSticker['bundle_name']}}</div>
                        <span style="padding-top: 40px;"></span>
                        <ul id="sortable">
                            @if($oneSticker['stickers'] == [])
                                <p class="alert alert-info">No stickers found!</p>
                            @else
                                <?php $i = 0;?>
                                @foreach($oneSticker['stickers'] as $childSticker)
                                    <li id="{{ $i }}" class="ui-state-default">
                                        <img class="img-responsive" src="{{$childSticker['sticker_url']}}" title="{{$childSticker['sticker_name']}}" style="height: 100%; width: 100%">
                                    </li>
                                    <?php $i++; ?>
                                @endforeach
                            @endif
                        </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <a href="/sticker/sticker-form/{{$oneSticker['bundle_name']}}" class="btn btn-default btn-block">Add A New Sticker</a>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>

        $(document).ready(function ()
        {
            $('ul').sortable({
                axis: 'x, y',
                update: function (event, ui) {
                    var data = $(this).sortable('toArray');
                    var token = '{{csrf_token()}}';
                    var bundleId = '{{$id}}';

                    $.ajax({
                        method: 'POST',
                        data: {'data': data, '_token': token, 'bundle_id' : bundleId},
                        type: 'POST',
                        url: '/sticker/sticker-sortable',
                        success: function(){
                            var index = 0;
                            $("#sortable").find('li').each(function(){
                                $(this).attr('id', index);
                                index++;
                            });
                        }

                    });
                }
            });
        });

    </script>
@endsection