@extends('layouts.app')

@section('content')

    <div class="container">
        <h2>Add a New Bundle</h2>
        {!! Form::open([
                'url' => '/sticker/bundle-form',
                'class' => 'form',
                'novalidate' => 'novalidate',
                'files' => true
            ]) !!}

            <div class="form-group">
                {!! Form::label('Bundle Name') !!}
                {!! Form::text('bundle_name', null, ["class"=>"form-control"]) !!}
            </div>

            <div class="form-group">
                {!! Form::label('Bundle Icon') !!}
                {!! Form::file('bundle_icon', null) !!}
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
        {!! Form::close() !!}
    </div>

@endsection