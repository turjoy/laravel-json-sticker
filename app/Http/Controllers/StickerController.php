<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Session;

class StickerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getStickers($id) 
    {
        $str = file_get_contents('sticker/sticker.json');

        $json = json_decode($str, true);

        $oneSticker = $json[$id];

        return view('sticker.show', compact('oneSticker', 'id'));
    }
    
    public function getBundleForm()
    {
        return view('sticker.create');
    }

    public function postBundleForm(Request $request)
    {
        $str = file_get_contents('sticker/sticker.json');

        if(empty($str) || is_null($str))
            $str = "[]";

        $json = json_decode($str, true);

        $data = array(
                    'bundle_name' => $request->get('bundle_name'),
                    'bundle_icon' => 'sticker/'.$request->get('bundle_name').'/'.$request->file('bundle_icon')->getClientOriginalName(),
                    'stickers' => array(

                    ));
        array_push($json, $data);

        $jsonData = json_encode($json, JSON_UNESCAPED_SLASHES);

        echo  file_put_contents("sticker/sticker.json", $jsonData);

        $request->file('bundle_icon')->move('sticker/'.$request->get('bundle_name'), $request->file('bundle_icon')->getClientOriginalName());

        return redirect()->back();
    }

    public function getStickerForm($bundleName)
    {
        $getBundleName = $bundleName;

        return view('child_sticker.create', compact('getBundleName'));
    }

    public function postStickerForm(Request $request, $specificBundleName)
    {
        $str = file_get_contents('sticker/sticker.json');

        $json = json_decode($str, true);

        $key = array_search($specificBundleName, array_column($json, 'bundle_name'));

        $stickers = array(
            'sticker_name' => $request->get('sticker_name'),
            'sticker_url' => '/sticker/'.$json[$key]['bundle_name'].'/'.$request->file('sticker_icon')->getClientOriginalName(),
            'sticker_mode' => $json[$key]['bundle_name']
        );

        array_push($json[$key]['stickers'], $stickers);

        $jsonData = json_encode($json, JSON_UNESCAPED_SLASHES);

        echo  file_put_contents("sticker/sticker.json", $jsonData);

        $request->file('sticker_icon')->move('sticker/'.$json[$key]['bundle_name'], $request->file('sticker_icon')->getClientOriginalName());

        return redirect()->back();
    }

    public function postBundleSortable(Request $request)
    {
        $jsonFileContent = file_get_contents('sticker/sticker.json');

        $jsonFileContent = json_decode($jsonFileContent, true);

        $data = $request->data;

        $total = count($data);

        $new_array = array();

        for ($i = 0; $i < $total; $i++)
        {
            $val = $data[$i];

            array_push($new_array, $jsonFileContent[$val]);
        }

        $encodedJson = json_encode($new_array, JSON_UNESCAPED_SLASHES);

        file_put_contents("sticker/sticker.json", $encodedJson);
    }

    public function postStickerSortable(Request $request)
    {
        $jsonFileContent = file_get_contents('sticker/sticker.json');

        $jsonFileContent = json_decode($jsonFileContent, true);

        $bundleId = $request->bundle_id;

        $data = $request->data;

        $dataCount = count($data);

        $newArray = $jsonFileContent;
        
        $newArray[$bundleId]['stickers'] = [];

        for ($i = 0; $i < $dataCount; $i++)
        {
            $value = $data[$i];

            array_push($newArray[$bundleId]['stickers'], $jsonFileContent[$bundleId]['stickers'][$value]);
        }



        $encodedJson = json_encode($newArray, JSON_UNESCAPED_SLASHES);

        file_put_contents("sticker/sticker.json", $encodedJson);
    }
}
