<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $str = file_get_contents('sticker/sticker.json');

        if(empty($str) || is_null($str))
            $str = "[]";

        $json = json_decode($str, true);

        return view('dashboard', compact('json'));
    }
    
    
}
